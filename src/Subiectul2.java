import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Subiectul2 {

    public static void main(String[] args) {

        new Window();
    }

}

class Window extends JFrame {


    JButton button;
    JTextArea textArea;

    Window() {

        setTitle("Subiectul2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200, 200);
        setVisible(true);


    }

    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;

        textArea = new JTextArea();
        textArea.setBounds(10, 45, 150, 80);

        button = new JButton("write");
        button.setBounds(10, 20, width, height);

        button.addActionListener(new ButtonListener());

        add(textArea);
        add(button);

    }

    class ButtonListener implements ActionListener {


        String fileContent;


        @Override
        public void actionPerformed(ActionEvent e) {
            String writtenContent = Window.this.textArea.getText();
            fileContent = "";
            fileContent = fileContent.concat(writtenContent);
            try {
                FileWriter writer = new FileWriter("src/writable.txt");
                writer.write(fileContent);
                writer.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }

        }
    }

}


