public class Subiectul1 {

    public static void main(String[] args) {

    }

}

interface Interface1 {

}

class A implements Interface1 {

    public M m;

    A() {
        m = new M(new B());
    }

    public void metA() {

    }

}

class M {

    public B b;
    M(B b) {
        this.b = b;
    }

}

class B {

    public void metB() {

    }

}

class L {
    private long t;
    public M m;
    public void f() {

    }
}

class X {
    public void i(L l) {

    }
}
